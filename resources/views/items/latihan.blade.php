@extends('adminlte.master2')

@section('judulFile')
  Halaman Latihan
@endsection

@section('judul1')
  <h1>Latihan dengan Study Case</h1>

@endsection

@section('isi')
    <div class = "row">
      <div class = "col-3">
        <div class="card" style="width: 15rem;">
          <img class="figure-img img-fluid rounded" style="height: 10rem;" src="{{asset('adminlte/dist/img/ilustrasi case study/airplane.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Airplane App</h5>
            <p class="card-text font-weight-light">Dibutuhkan aplikasi mobile yang dapat mempermudah user dalam booking seat pesawat tanpa perlu repot mengantri.</p>
            <a href="/detailLatihan1" class="btn btn-success btn-block">Detail Study Case</a>
          </div>
        </div>
      </div>  

      <div class = "col-3">
        <div class="card" style="width: 15rem;">
          <img class="figure-img img-fluid rounded" style="height: 10rem;" src="{{asset('adminlte/dist/img/ilustrasi case study/onlinecourse.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Online Course Web</h5>
            <p class="card-text font-weight-light">Pada Study Case ini, kamu harus membuat web yang diperuntukkan untuk orang yang mau memperdalam mengenai Coding.</p>
            <a href="/detailLatihan2" class="btn btn-success btn-block">Detail Study Case</a>
          </div>
        </div>
      </div>  

      <div class = "col-3">
        <div class="card" style="width: 15rem;">
        <img class="figure-img img-fluid rounded" style="height: 10rem;" src="{{asset('adminlte/dist/img/ilustrasi case study/shop.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Aplikasi Self Service belanja</h5>
            <p class="card-text font-weight-light">Bagaimana ya cara membuat mobile apps untuk orang yang ingin melakukan selfservice saat berbelanja?</p>
            <a href="/detailLatihan3" class="btn btn-success btn-block">Detail Study Case</a>
          </div>
        </div>
      </div>  

      <div class = "col-3">
        <div class="card" style="width: 15rem;">
        <img class="figure-img img-fluid rounded" style="height: 10rem;" src="{{asset('adminlte/dist/img/ilustrasi case study/motor.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Aplikasi Pelacak Rental motor</h5>
            <p class="card-text font-weight-light">Aplikasi mobile yang dapat mempermudah user dalam booking seat pesawat tanpa perlu repot mengantri.</p>
            <a href="/detailLatihan4" class="btn btn-success btn-block">Detail Study Case</a>
          </div>
        </div>
      </div>  

      <div class = "col-3 mt-3">
        <div class="card" style="width: 15rem;">
        <img class="figure-img img-fluid rounded" style="height: 10rem;" src="{{asset('adminlte/dist/img/ilustrasi case study/wash car.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Aplikasi tempat cuci mobil</h5>
            <p class="card-text font-weight-light">Ketika seseorang pergi ke tempat cuci mobil, tentu saja ada kemungkinan tempat cuci mobil tersebut sangat ramai.</p>
            <a href="/detailLatihan5" class="btn btn-success btn-block">Detail Study Case</a>
          </div>
        </div>
      </div> 
      
      <div class = "col-3 mt-3">
        <div class="card" style="width: 15rem;">
        <img class="figure-img img-fluid rounded" style="height: 10rem;" src="{{asset('adminlte/dist/img/ilustrasi case study/time.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Aplikasi Pengelola waktu</h5>
            <p class="card-text font-weight-light">Hmmm... Time is money, sepertinya dibutuhkan aplikasi pengelola waktu yang dapat membuat hari jadi lebih teratur. Mari kita buat!</p>
            <a href="/detailLatihan6" class="btn btn-success btn-block">Detail Study Case</a>
          </div>
        </div>
      </div>  

      <div class = "col-3 mt-3">
        <div class="card" style="width: 15rem;">
        <img class="figure-img img-fluid rounded" style="height: 10rem;" src="{{asset('adminlte/dist/img/ilustrasi case study/usaha.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Wirausaha & Teknologi</h5>
            <p class="card-text font-weight-light">Dibutuhkan aplikasi mobile yang dapat mempermudah user dalam booking seat pesawat tanpa perlu repot mengantri.</p>
            <a href="/detailLatihan7" class="btn btn-success btn-block">Detail Study Case</a>
          </div>
        </div>
      </div>  

      <div class = "col-3 mt-3">
        <div class="card hoverable" style="width: 15rem;">
        <img class="figure-img img-fluid rounded" style="height: 10rem;" src="{{asset('adminlte/dist/img/ilustrasi case study/sosmed.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Sosial Media Web</h5>
            <p class="card-text font-weight-light">Pada Study Case ini, kamu harus membuat web sosial media yang dapat disukai oleh anak muda karena ada unsur kekinian.</p>
            <a href="/detailLatihan8" class="btn btn-success btn-block">Detail Study Case</a>
          </div>
        </div>
      </div>  

 

    </div>

</div>


@endsection