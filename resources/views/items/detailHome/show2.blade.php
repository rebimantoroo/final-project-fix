@extends('adminlte.master')

@section('judulFile')
  Project
@endsection

@section('judul1')
Show Off Project ESD Study Group
@endsection

@section('judul2')
Travel
@endsection

@section('isi')
  <div>
  <img class="card-img-top"  src="{{asset('adminlte/dist/img/web.png')}}" alt="Card image cap">
  <br> <br>
  <h5 class="font-weight-bold">Penjelasan</h5>
  <p>Aplikasi booking travel online</p>

  <h5 class="font-weight-bold">Tools Yang digunakan</h5>
  <p>Figma, MazeDesign</p>
  </div>

  <div class="card-body">
    <div class="media">
      <img alt="" class="img-circle mr-3" id="practitioner_image" src="{{asset('adminlte/dist/img/blank.png')}}" class="main_badge" title="" style="height:50px; weight:50px;">
      <div class="media-body">
        <a class="mt-0 font-weight-normal">Dita Cahya</a>
        <p class="font-weight-light">SoftDev Study Group</p>
      </div>
    </div>
@endsection