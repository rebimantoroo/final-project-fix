@extends('adminlte.master3')

@section('judulFile')
Project Riset Memenuhi kebutuhan Mahasiswa
@endsection

@section('judul1')
<h1><a href="/project">Project</a>/Project Riset Memenuhi kebutuhan Mahasiswa</>
@endsection


@section('isi')
  <div class = "row ">
    <div class = "col-8 ">
      <img class="figure-img img-fluid rounded mx-auto d-block" style="height: 20rem; " src="{{asset('adminlte/dist/img/22.jpg')}}" alt="Card image cap">
      <p>Pada Study Case ini, kamu harus membuat web yang diperuntukkan untuk orang yang mau memperdalam mengenai Coding dengan spesifikasi minimum sebagai berikut :
        </p>

      <ul>
      
        <li>Menggunakan Laravel versi 6 atau versi 6 keatas</li>
        <li>Minimum terdapat 3 fitur CRUD (Misalkan jika temanya Portal Berita maka minimal ada CRUD Berita, CRUD tag, CRUD profile penulis, dst.)</li>		
        <li>Template tampilan dapat memilih sendiri, pilihlah template yang cocok untuk tema project dan gratis</li>
        <li>Harus menggunakan Authentication (Laravel Auth)</li>
        <li>Terdapat fitur yang menggunakan Eloquent Relationship (One To One, One To Many, Many To Many)</li>
        <li>Minimum memasangkan 3 Library/Package di luar Laravel Contohnya : sweet alert, laravel excel, laravel pdf, laravel file manager, Datatables, TinyMCE, dll.</li>
      </ul>

    </div>

    <div class = "col-4">
    <p>Yuk, bangun portfolio datamu dengan menyelesaikan EStudy Case Study untuk mengasah skill kamu lebih dalam. Baik itu dalam Design, Programming, maupun Technopreneur.</p>
        <h5 class="font-weight-bold">Penjelasan</h5>
        <p>Dibutuhkan aplikasi mobile yang dapat mempermudah user dalam booking seat pesawat tanpa perlu repot mengantri.</p>
        <h5 class="font-weight-bold">Tools Yang dibutuhkan : </h5>
        <p>Figma, Visual Studio Code, Boostrap CSS, Laravel, PHP</p>
</div>


@endsection




