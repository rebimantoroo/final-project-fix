<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">

    <title>E-StuDy</title>
  </head>
  <body>
    <div class= container >
        <div>
          <nav class="navbar navbar-light bg-white">
            <a class="navbar-brand"> <img src="{{asset('adminlte/dist/img/AdminLTELogo.png')}}" alt=""width="100px" > <b>E-StuDy </a>
            <ul class="nav">
              <li class="nav-item">
                <a class="nav-link text-dark" href="#" >Study Group</a>
              </li>
              <li class="nav-item">
                <a class="nav-link text-dark" href="#">Project</a>
              </li>
              <li class="nav-item">
                <a class="nav-link text-dark" href="#">About</a>
              </li>
            </ul>
          </nav>
        </div>

        <div class="row d-flex justify-content-between" >
          <div class="col mb-5 mt-5" >
            <h1>E-StuDy</h1>
            <h7 class="font-weight-light">For the better ESD</h7>
            <p>Web E-StuDy merupakan aplikasi yang membantu Study Group yang ada di Lab ESD 
              dalam pelaksanaan kegiatan lab yang ada,
              terutama dalam bidang pembelajaran dan pencarian tim project. 

              <br>
              <br>

            <a class="btn btn-primary btn-success" href="/login" role="button">Login</a>
          </div>
          <div class="col mb-5 mt-1">
            <img src="{{asset('adminlte/dist/img/Webinar-pana.png')}}" width =400px>
          </div>
        </div>
    </div>
  </body>
</html>

