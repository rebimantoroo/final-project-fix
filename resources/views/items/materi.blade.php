@extends('adminlte.master2')

@section('judulFile')
  Halaman Materi
@endsection

<!--Roadmap Materi-->
@section('judul1')
  Roadmap Materi
@endsection

@section('isi')
    <div class = "row">
      <div class = "col-md-3">
        <div class="card" style="width: 15rem;">
          <img class="card-img-top img-circle ml-3 mt-3"  style="width: 4rem;" src="{{asset('adminlte/dist/img/icon/design.png')}}" alt="Card image cap">
          <div class="card-body">
          <a href="/isiMateri1" class="card-title font-weight-bold" style="color: #2D3E50">Design</a>
          <p class="card-text font-weight-light" style="color: #2D3E50">Alur belajar Design aplikasi.</p>
          </div>
        </div>
      </div>   

      <div class = "col-md-3">
        <div class="card" style="width: 15rem;">
          <img class="card-img-top img-circle ml-3 mt-3"  style="width: 4rem;" src="{{asset('adminlte/dist/img/icon/apps.png')}}" alt="Card image cap">
          <div class="card-body">
          <a href="/isiMateri2" class="card-title font-weight-bold" style="color: #2D3E50">Mobile Developer</a>
          <p class="card-text font-weight-light" style="color: #2D3E50">Alur Belajar Aplikasi Mobile.</p>
          </div>
        </div>
      </div> 

      <div class = "col-md-3">
        <div class="card" style="width: 15rem;">
          <img class="card-img-top img-circle ml-3 mt-3"  style="width: 4rem;" src="{{asset('adminlte/dist/img/icon/web.png')}}" alt="Card image cap">
          <div class="card-body">
          <a href="/isiMateri3" class="card-title font-weight-bold" style="color: #2D3E50">Web Developer</a>
          <p class="card-text font-weight-light" style="color: #2D3E50">Alur Belajar Web.</p>
          </div>
        </div>
      </div> 

      <div class = "col-md-3">
        <div class="card" style="width: 15rem;">
          <img class="card-img-top img-circle ml-3 mt-3"  style="width: 4rem;" src="{{asset('adminlte/dist/img/icon/techno.png')}}" alt="Card image cap">
          <div class="card-body">
          <a href="/isiMateri4" class="card-title font-weight-bold" style="color: #2D3E50">Technopreneur</a>
          <p class="card-text font-weight-light" style="color: #2D3E50">Alur Belajar Technopreneur.</p>
          </div>
        </div>
      </div> 
 
    </div>

@endsection

<!--Top 3 Course-->
@section('judul2')
  Top 3 Course
@endsection

@section('isi2')
<div class = "row">
      <div class = "col">
        <div class="card" style="width: 20rem;">
          <img class="card-img-top"  src="{{asset('adminlte/dist/img/Developer activity-amico.png')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Front End Developer</h5>
            <p class="card-text font-weight-light"> Bahasa mark up seperti HTML dan CSS serta bahasa pemrograman JavaScript. JavaScript sendiri mempunyai banyak library dan framework..</p>
            <a href="/detailMateri1" class="btn btn-dark" >Pelajari Materi</a>
          </div>
        </div>
      </div>   

      <div class = "col">
        <div class="card" style="width: 20rem;">
          <img class="card-img-top"  src="{{asset('adminlte/dist/img/Programming-amico.png')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Back End Developer</h5>
            <p class="card-text font-weight-light"> Seorang Back-End Developer adalah Software Developer yang bertanggung jawab dalam mengelola server, aplikasi, dan juga database didalamnya.</p>
            <a href="/detailMateri2" class="btn btn-dark">Pelajari Materi</a>
          </div>
        </div>
      </div> 


      <div class = "col">
        <div class="card" style="width: 20rem;">
          <img class="card-img-top"  src="{{asset('adminlte/dist/img/amico.png')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">FullStack Developer</h5>
            <p class="card-text font-weight-light"> Full stack developer adalah posisi programmer dimana orang tersebut telah menguasai pemrograman backend sekaligus paham teknologi frontend</p>
            <a href="/detailMateri3" class="btn btn-dark">Pelajari Materi</a>
          </div>
        </div>
      </div> 
    </div>

@endsection


<!--Success Stories-->
@section('judul3')
  Success Stories
@endsection

@section('isi3')
    <div class = "row">
      <div class = "col">
        <div class="card" style="width: 20rem;">
          <img class="card-img-top img-circle ml-3 mt-3 "  style="width: 4rem;" src="{{asset('adminlte/dist/img/userreview/review.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <p class="card-title font-weight-bold" style="color: #2D3E50">Tri Prastia</p>
            <p class="card-text font-weight-light text-muted" style="color: #2D3E50">Pelajar</p>
            <p class="card-text font-weight-light" style="color: #2D3E50">Mentornya keren cara menyampaikan setiap materinya juga detail dan mudah dipahami.</p>

          </div>
        </div>
      </div>   

      <div class = "col">
        <div class="card" style="width: 20rem;">
          <img class="card-img-top img-circle ml-3 mt-3 "  style="width: 4rem;" src="{{asset('adminlte/dist/img/userreview/review1.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <p class="card-title font-weight-bold" style="color: #2D3E50">M Reza Saputra</p>
            <p class="card-text font-weight-light text-muted" style="color: #2D3E50">Mahasiswa</p>
            <p class="card-text font-weight-light" style="color: #2D3E50">Kelas yang paling recommended pokoknya buat investasi ilmu di era revolusi industri 4.0.</p>

          </div>
        </div>
      </div>   

      <div class = "col">
        <div class="card" style="width: 20rem;">
          <img class="card-img-top img-circle ml-3 mt-3 "  style="width: 4rem;" src="{{asset('adminlte/dist/img/userreview/review2.jpg')}}" alt="Card image cap">
          <div class="card-body">
            <p class="card-title font-weight-bold" style="color: #2D3E50">Chaerul Marwan</p>
            <p class="card-text font-weight-light text-muted" style="color: #2D3E50">Web Developer</p>
            <p class="card-text font-weight-light" style="color: #2D3E50">Kelas yang sangat bermanfaat dan ilmu up-to-date dan yang paling penting biayanya terjangkau.</p>

          </div>
        </div>
      </div>  
      


      




 
    </div>

@endsection
