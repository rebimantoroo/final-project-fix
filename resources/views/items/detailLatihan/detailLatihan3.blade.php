@extends('adminlte.master3')

@section('judulFile')
  Aplikasi Pelacak Rental Motor
@endsection

@section('judul1')
<h1><a href="/latihan">Latihan</a>/Aplikasi Pelacak Rental Motor</>
@endsection


@section('isi')
  <div class = "row ">
    <div class = "col-8 ">
      <img class="figure-img img-fluid rounded mx-auto d-block" style="height: 20rem; " src="{{asset('adminlte/dist/img/ilustrasi case study/shop.jpg')}}" alt="Card image cap">
      <p>Pada study kasus kali ini, kamu diminta untuk merancang sebuah aplikasi yang dapat mempermudah user dalam booking seat pesawat tanpa perlu repot mengantri dalam bentuk aplkasi mobile dengan spesifikasi sebagai berikut :
        </p>

      <ul>
        <li><a>Terdapat fitur akun yang dapat memudahkan user dalam mengganti data diri</a></li>
        <li><a>Terdapat filter yang dapat digunakan user dalam mensortir barang yang diinginkan</a></li>		
      </ul>

    </div>

    <div class = "col-4">
    <p>Yuk, bangun portfolio datamu dengan menyelesaikan EStudy Case Study untuk mengasah skill kamu lebih dalam. Baik itu dalam Design, Programming, maupun Technopreneur.</p>
        <h5 class="font-weight-bold">Penjelasan</h5>
        <p>mobile yang dapat mempermudah user dalam booking seat pesawat tanpa perlu repot mengantri.</p>
        <h5 class="font-weight-bold">Tools Yang dibutuhkan : </h5>
        <p>Figma, Visual Studio Code, Boostrap CSS, Laravel, PHP</p>
</div>


@endsection




