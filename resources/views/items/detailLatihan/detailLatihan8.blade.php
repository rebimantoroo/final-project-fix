@extends('adminlte.master3')

@section('judulFile')
  Airplane App
@endsection

@section('judul1')
<h1><a href="/latihan">Latihan</a>/Airplane App</>
@endsection


@section('isi')
  <div class = "row ">
    <div class = "col-8 ">
      <img class="figure-img img-fluid rounded mx-auto d-block" style="height: 20rem; " src="{{asset('adminlte/dist/img/ilustrasi case study/sosmed.jpg')}}" alt="Card image cap">
      <p>Pada study kasus kali ini, kamu diminta untuk membuat sebuah aplikasi yang dapat mempermudah user dalam booking seat pesawat tanpa perlu repot mengantri dalam bentuk aplkasi mobile dengan spesifikasi sebagai berikut :
        </p>

    <ul>
      <li>User harus terdaftar di web untuk menggunakan layanan sosmed</li>
      <li>User dapat membuat postingan berupa: tulisan, gambar dengan caption, quote.</li>
      <li>User dapat membuat, mengedit, dan menghapus pada postingan milik sendiri.</li>
      <li>Seorang User dapat mengikuti (follow) ke banyak User lainnya. Seorang User dapat diikuti oleh banyak User lainnya.</li>
      <li>Satu postingan dapat disukai(like) oleh banyak User. Satu User dapat menyukai (like) banyak Postingan.</li>
      <li>Satu postingan dapat memiliki banyak komentar. Satu komentar dapat dikomentari oleh banyak user.</li>
      <li>Satu komentar dapat disukai oleh banyak User. Satu User dapat menyukai banyak komentar.</li>
      <li>Seorang User dapat membuat dan mengubah profile nya sendiri.</li>
      <li>Pada halaman menampilkan postingan terdapat konten posting, komentar, jumlah komentar, jumlah like.</li>
      <li>Pada halaman profile User terdapat biodata, jumlah user pengikut(follower), jumlah user yang diikuti (following)</li>
    </ul>

    </div>

    <div class = "col-4">
    <p>Yuk, bangun portfolio datamu dengan menyelesaikan EStudy Case Study untuk mengasah skill kamu lebih dalam. Baik itu dalam Design, Programming, maupun Technopreneurship.</p>
        <h5 class="font-weight-bold">Penjelasan</h5>
        <p>Dibutuhkan aplikasi mobile yang dapat mempermudah user dalam booking seat pesawat tanpa perlu repot mengantri.</p>
        <h5 class="font-weight-bold">Tools Yang dibutuhkan : </h5>
        <p>Figma, Visual Studio Code, Boostrap CSS, Laravel, PHP</p>
</div>


@endsection




