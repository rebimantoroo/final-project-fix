@extends('adminlte.master2')

@section('judulFile')
  Halaman Materi
@endsection

@section('judul1')
  Web Developer Course
@endsection


@section('isi')
    <div class = "row">
      <div class = "col">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top"  src="{{asset('adminlte/dist/img/Developer activity-amico.png')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Front End Developer</h5>
            <p class="card-text font-weight-light"> Bahasa mark up seperti HTML dan CSS serta bahasa pemrograman JavaScript. JavaScript sendiri mempunyai banyak library dan framework..</p>
            <a href="/detailMateri1" class="btn btn-dark btn-success">Pelajari Materi</a>
          </div>
        </div>
      </div>   

      <div class = "col">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top"  src="{{asset('adminlte/dist/img/Programming-amico.png')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">Back End Developer</h5>
            <p class="card-text font-weight-light"> Seorang Back-End Developer adalah Software Developer yang bertanggung jawab dalam mengelola server, aplikasi, dan juga database didalamnya.</p>
            <a href="/detailMateri2" class="btn btn-dark btn-success">Pelajari Materi</a>
          </div>
        </div>
      </div> 


      <div class = "col">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top"  src="{{asset('adminlte/dist/img/amico.png')}}" alt="Card image cap">
          <div class="card-body">
            <h5 class="card-title font-weight-bold">FullStack Developer</h5>
            <p class="card-text font-weight-light"> Full stack developer adalah posisi programmer dimana orang tersebut telah menguasai pemrograman backend sekaligus paham teknologi frontend</p>
            <a href="/detailMateri3" class="btn btn-dark btn-success">Pelajari Materi</a>
          </div>
        </div>
      </div> 
    </div>

@endsection


