@extends('adminlte.master')
@section('judulFile')
  Halaman Mentoring
@endsection

@section('judul1')
<p> <a href="/mentoring">Mentoring/</a>Show</p> 
@endsection

@section('judul2')
<h1>Detail Project </h1>
@endsection

@section('judul2')
<h1>Project ID : {{$mentoring->id}} </h1>
@endsection

@section('isi')
    <div class="mt-3 ml-3">
        <h1> {{$mentoring->nama}}</h1><br>
        <h2>Deskripsi : <br></h2>
        <p>{{$mentoring->deskripsi}}</p>
        <p>Link : <a href="{{$mentoring->link}}"> Klik Disini</a></p>
        <p>Author : {{$mentoring->mentoring->name}} </p>
      </div>

@endsection