@extends('adminlte.master')

@section('judulFile')
  Halaman Mentoring
@endsection

@section('judul1')
<p> <a href="/mentoring">Mentoring/</a>Edit</p> 
@endsection

@section('judul2')
    Silahkan lakukan perubahan terhadap project
@endsection

@section('isi')
<div class="card card-primary ml-3 mt-3">
              <div class="card-header">
                <h3 class="card-title">Edit Project</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/mentoring/{{$mentoring->id}}" method="POST">
                @csrf
                @method('PUT')                
                <div class="card-body">
                  <div class="form-group">
                    <label for="nama">Nama Project</label>
                    <input type="text" class="form-control" id="nama" name="nama" value="{{old('nama',$mentoring->nama)}}" placeholder="Masukan Nama Project">
                    @error('nama')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror  
                </div>
                  <div class="form-group">
                    <label for="deskripsi">Deskripsi</label>
                    <input type="text" class="form-control" id="deskripsi" name="deskripsi" value="{{old('deskripsi',$mentoring->deskripsi)}}" placeholder="Masukan deskripsi">
                    @error('deskripsi')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror    
                </div>

                  <div class="form-group">
                    <label for="link">Link</label>
                    <input type="text" class="form-control" id="link" name="link" value="{{old('link',$mentoring->link)}}" placeholder="Masukan link">
                    @error('link')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror    
                </div>
                  
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Edit</button>
                </div>
              </form>
            </div>
@endsection