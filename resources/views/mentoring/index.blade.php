@extends('adminlte.master')

@section('judulFile')
  Halaman Mentoring
@endsection

@section('judul1')
  Mentoring
@endsection

@section('judul2')
    Riwayat Mentoring Project
@endsection

@section('isi')
    <div class="mt-3 ml-3">
    <div class="card">
              <div class="card-header">
                <h3 class="card-title">Tabel Mentoring Project</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                @if(session('berhasil'))
                    <div class="alert alert-success">
                        {{session('berhasil')}}
                    </div>
                @endif
                <a class="btn btn-primary mb-sm-2" href="/mentoring/create">Tambah Project Baru</a>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>User ID</th>
                      <th>Nama Project</th>
                      <th>Actions</th>
                      <th>Feedback</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($mentoring as $key => $mentoring)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$mentoring->users_id}}</td>
                            <td>{{$mentoring->nama}}</td>
                            <td style="display:flex">
                                <a href="/mentoring/{{$mentoring->id}}" class="btn btn-info btn-sm">show</a>
                                <a href="/mentoring/{{$mentoring->id}}/edit" class="btn btn-default btn-sm">edit</a>
                                <form action="/mentoring/{{$mentoring->id}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" value="delete" class="btn btn-danger btn-sm">
                                </form>
                            </td>
                            <td>
                                <a href="#">Klik disini</a>
                            </td>
                        </tr>
                    @empty 
                        <td colspan="5" align="center">Data Kosong</td>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
    </div>
@endsection