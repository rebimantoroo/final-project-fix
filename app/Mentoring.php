<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mentoring extends Model
{
    protected $table="mentoring";
    protected $fillable =["nama","deskripsi","link","users_id"];
    
    public function mentoring(){
        return $this->belongsTo('App\User','users_id');
    }

}
