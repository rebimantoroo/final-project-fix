<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class detailLatihan extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function detailLatihan1(){
        return view('items.detailLatihan.detailLatihan1');
    }
    
    public function detailLatihan2(){
        return view('items.detailLatihan.detailLatihan2');
    }

    public function detailLatihan3(){
        return view('items.detailLatihan.detailLatihan3');
    }

    public function detailLatihan4(){
        return view('items.detailLatihan.detailLatihan4');
    }

    public function detailLatihan5(){
        return view('items.detailLatihan.detailLatihan5');
    }

    public function detailLatihan6(){
        return view('items.detailLatihan.detailLatihan6');
    }
    
    public function detailLatihan7(){
        return view('items.detailLatihan.detailLatihan7');
    }
    
    public function detailLatihan8(){
        return view('items.detailLatihan.detailLatihan8');
    }
}
