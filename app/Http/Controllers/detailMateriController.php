<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class detailMateriController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function detailMateri1(){
        return view('items.detailMateri.detailMateri1');
    }
    
    public function detailMateri2(){
        return view('items.detailMateri.detailMateri2');
    }

    public function detailMateri3(){
        return view('items.detailMateri.detailMateri3');
    }

    public function detailMateri4(){
        return view('items.detailMateri.detailMateri4');
    }

    public function detailMateri5(){
        return view('items.detailMateri.detailMateri5');
    }

    public function detailMateri6(){
        return view('items.detailMateri.detailMateri6');
    }

    

}
