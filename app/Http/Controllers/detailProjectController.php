<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class detailProjectController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function detailProject1(){
        return view('items.detailProject.detailProject1');
    }
    
    public function detailProject2(){
        return view('items.detailProject.detailProject2');
    }

    public function detailProject3(){
        return view('items.detailProject.detailProject3');
    }

    public function detailProject4(){
        return view('items.detailProject.detailProject4');
    }

    public function detailProject5(){
        return view('items.detailProject.detailProject5');
    }

    public function detailProject6(){
        return view('items.detailProject.detailProject6');
    }
}
