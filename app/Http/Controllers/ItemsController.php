<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ItemsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function materi(){
        return view('items.materi');
    }
    
    public function latihan(){
        return view('items.latihan');
    }
    
    public function project(){
        return view('items.project');
    }
}
