<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Mentoring;
use App\User;
use Auth;

class MentoringController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create(){
        return view('mentoring.create');
    }

    public function store(Request $request){
        $request->validate([
            "nama"=> 'required|unique:mentoring',
            "deskripsi"=> 'required',
            "link"=> 'required'
        ]);
        $mentoring = Mentoring::create([
            "nama"=> $request["nama"],
            "deskripsi"=> $request["deskripsi"],
            "link"=> $request["link"],
            "users_id"=>Auth::id()
        ]);
        return redirect('mentoring')->with('berhasil','Project berhasil ditambahkan');
    }

    public function index() {
        $mentoring = Mentoring::all();
        return view('mentoring.index',compact('mentoring'));
    }

    public function show($id){
        $mentoring = Mentoring::find($id);
        return view('mentoring.show',compact('mentoring'));
    }

    public function edit($id){
        $mentoring = Mentoring::find($id);

        return view('mentoring.edit',compact('mentoring'));
    }

    public function update($id,Request $request){
        $request->validate([
            "nama"=> 'required',
            "deskripsi"=> 'required',
            "link"=> 'required'
        ]);

        $update = Mentoring::where('id',$id)->update([
            "nama" => $request["nama"],
            "deskripsi"=> $request["deskripsi"],
            "link"=> $request["link"]
        ]);

        return redirect('/mentoring')->with('berhasil','Berhasil update data Project');
    }

    public function destroy($id) {
        Mentoring::destroy($id);
        return redirect('mentoring')->with('berhasil','Data project berhasil dihapus');

    }
}
