# Final Project

## Kelompok 14

## Anggota :
* Muhammad Rizal Bimantoro
* Pradita Cahyani

## Tema Project
Online Course

## ERD
![ERD-OnlineCourse.png](ERD-OnlineCourse.png)

## Link Video
Link Demo Aplikasi : https://youtu.be/HRiXg_zSEeg
<br>
Link Deploy : https://stormy-sea-00121.herokuapp.com/