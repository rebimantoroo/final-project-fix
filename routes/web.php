<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
//items
Route::get('/materi', 'ItemsController@materi');
Route::get('/latihan', 'ItemsController@latihan');
Route::get('/project', 'ItemsController@project');
//detailMateri
Route::get('/detailMateri1', 'detailMateriController@detailMateri1');
Route::get('/detailMateri2', 'detailMateriController@detailMateri2');
Route::get('/detailMateri3', 'detailMateriController@detailMateri3');
Route::get('/detailMateri4', 'detailMateriController@detailMateri4');
Route::get('/detailMateri5', 'detailMateriController@detailMateri5');
Route::get('/detailMateri6', 'detailMateriController@detailMateri6');
//detailHome
Route::get('/show1', function () {
    return view('items.detailHome.show1');
});
Route::get('/show2', function () {
    return view('items.detailHome.show2');
});
Route::get('/show3', function () {
    return view('items.detailHome.show3');
});
//detailLatihan
Route::get('/detailLatihan1', 'detailLatihan@detailLatihan1');
Route::get('/detailLatihan2', 'detailLatihan@detailLatihan2');
Route::get('/detailLatihan3', 'detailLatihan@detailLatihan3');
Route::get('/detailLatihan4', 'detailLatihan@detailLatihan4');
Route::get('/detailLatihan5', 'detailLatihan@detailLatihan5');
Route::get('/detailLatihan6', 'detailLatihan@detailLatihan6');
Route::get('/detailLatihan7', 'detailLatihan@detailLatihan7');
Route::get('/detailLatihan8', 'detailLatihan@detailLatihan8');
//detailProject
Route::get('/detailProject1', 'detailProjectController@detailProject1');
Route::get('/detailProject2', 'detailProjectController@detailProject2');
Route::get('/detailProject3', 'detailProjectController@detailProject3');
Route::get('/detailProject4', 'detailProjectController@detailProject4');
Route::get('/detailProject5', 'detailProjectController@detailProject5');
Route::get('/detailProject6', 'detailProjectController@detailProject6');

Route::resource('mentoring','MentoringController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
